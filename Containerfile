# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

ARG ALPINE_VERSION="latest"

FROM index.docker.io/library/alpine:${ALPINE_VERSION}

LABEL maintainer="Olliver Schinagl <oliver@schinagl.nl>"

ARG ALPINE_VERSION="latest"
ARG ALPINE_CROSS_ARCH="armv7"
ARG CROSS_COMPILE_ARCH="arm"
ARG TARGET_ARCH="arm32v7"

ENV ALPINE_VERSION="${ALPINE_VERSION}"
ENV CROSS_COMPILE="${CROSS_COMPILE_ARCH}-none-eabi-"
ENV ALPINE_CROSS_ARCH="${ALPINE_CROSS_ARCH}"
ENV TARGET_ARCH="${TARGET_ARCH}"

COPY "./containerfiles/buildenv_check.sh" "/test/buildenv_check.sh"

RUN \
    apk add --no-cache \
        'bison' \
        'bzip2' \
        'cpio' \
        'dtc' \
        'findutils' \
        'flex' \
        'gcc' \
        'gcc-cross-embedded' \
        'git' \
        'gzip' \
        'kmod' \
        'lz4' \
        'lzop' \
        'make' \
        'musl-dev' \
        'ncurses-dev' \
        'openssl-dev' \
        'perl' \
        'xz' \
    ;

# Alpine does not allow adding packages with different --arch (no multi-arch)
# So instead, we bootstrap an arm32v7 directory, caching the apk files and
# install those into the host.
# hadolint ignore=DL3019
RUN \
    _alpine_root="$(mktemp -d -p "${TMPDIR:-/tmp}" 'alpine_root.XXXXXX')" && \
    _klibc_root="$(mktemp -d -p "${TMPDIR:-/tmp}" 'klibc.XXXXXX')" && \
    apk \
        --allow-untrusted \
        --arch "${ALPINE_CROSS_ARCH}" \
        --cache-dir="${_klibc_root}" \
        --initdb \
        --repository='http://dl-cdn.alpinelinux.org/alpine/edge/testing' \
        --repository='http://dl-cdn.alpinelinux.org/alpine/edge/community' \
        --repository="http://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION}/community" \
        --repository="http://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION}/main" \
        --repository='http://dl-cdn.alpinelinux.org/alpine/latest-stable/main' \
        --root "${_alpine_root?}" \
        add klibc && \
    apk --allow-untrusted add "${_klibc_root}/"*'klibc'*'.apk' && \
    rm -f -r "${_alpine_root:?}" "${_klibc_root:?}"
