#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu
if [ -n "${DEBUG_TRACE_SH:-}" ] && \
   [ "${DEBUG_TRACE_SH:-}" != "${DEBUG_TRACE_SH#*"$(basename "${0}")"*}" ] || \
   [ "${DEBUG_TRACE_SH:-}" = 'all' ]; then
	set -x
fi

DEF_CONTAINER_RUNTIME='docker'
DEF_CONTAINER_USE_CACHE='yes'
WORKDIR="${WORKDIR:-/workdir}"
REQUIRED_COMMANDS='
	[
	basename
	command
	echo
	eval
	exit
	id
	nproc
	printf
	pwd
	readlink
	test
	trap
	whoami
'

usage() {
	echo "Usage: ${0} [COMMAND] [ARGUMENTS]"
	echo 'Container wrapper and helper script.'
	echo 'The following environment variables can be set to pass additional parameters'
	echo "    [CONTAINER_RUNTIME] to change the container runtime to use (default: ${DEF_CONTAINER_RUNTIME})"
	echo "    [CONTAINER_USE_CACHE] use container build cache when building container (default: ${DEF_CONTAINER_USE_CACHE})"
	echo '    [CI_REGISTRY_IMAGE] to change the container to build in'
	echo '    [EXTRA_REPO] extra repository to use to pull repositories from'
	echo '    [OPT_CONTAINER_ARGS] optional arguments to supply to the container run command'
	echo
	echo 'When symlinking using the name of an existing script, prefixed with "container_"'
	echo 'run that inside the container. E.g. container_run_test.sh runs run_test.sh inside the container.'
	echo 'By using the "docker_" prefix, docker runtime is forced.'
	echo
	echo 'Without any commands and without a symlink will open a console in the container.'
}

_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

init()
{
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM

	src_file="$(readlink -f "${0}")"
	src_dir="${src_file%%"${src_file##*'/'}"}"
	src_name="$(basename "${0}")"

	if [ "${src_name%'_'*}" != "${src_name}" ]; then
		container_runtime="$(command -v "${src_name%'_'*}")"
	fi
	container_runtime="${container_runtime:-${CONTAINER_RUNTIME:-${DEF_CONTAINER_RUNTIME:?}}}"

	fake_whoami="$(mktemp)"
	cat > "${fake_whoami}" <<-EOT
		#/bin/sh

		if [ "\$(id -u)" -ge 1000 ]; then
		    echo "$(whoami || true)
		else
		    whoami "\${@}"
		fi

		exit 0
	EOT
	chmod +x "${fake_whoami}"

	# shellcheck disable=SC2021  # Busybox tr is non-posix without classes
	ci_registry_image="${ci_registry_image:-${CI_REGISTRY_IMAGE:-$(basename "$(pwd || true| \
	                                                               tr '[A-Z]' '[a-z]' || true)"):latest}}"

	if ! "${container_runtime}" image pull "${ci_registry_image}" 2> '/dev/null'; then
		e_warn "Unable to pull container image '${ci_registry_image}', building locally instead."
		if [ "${CONTAINER_USE_CACHE:-${DEF_CONTAINER_USE_CACHE}}" = 'yes' ]; then
			unset _cache
		else
			_cache='no'
		fi

		_containerfile="${src_dir}/Dockerfile"
		if [ ! -f "${_containerfile}" ]; then
			_containerfile="${src_dir}/Containerfile"
		fi
		if [ ! -f "${_containerfile}" ]; then
			e_err 'Unable to find a containerfile.'
		fi

		if ! "${container_runtime}" image build \
		            ${_cache:+--no-cache} \
		            ${ALPINE_CROSS_ARCH:+--build-arg "ALPINE_CROSS_ARCH=${ALPINE_CROSS_ARCH}"} \
		            ${ALPINE_VERSION:+--build-arg "ALPINE_VERSION=${ALPINE_VERSION}"} \
		            ${CROSS_COMPILE_ARCH:+--build-arg "CROSS_COMPILE_ARCH=${CROSS_COMPILE_ARCH}"} \
		            ${TARGET_ARCH:+--build-arg "TARGET_ARCH=${TARGET_ARCH}"} \
			    --file "${_containerfile}" \
		            --pull \
		            --rm \
		            --tag "${ci_registry_image}" \
		            "${src_dir}"; then
			e_warn 'Failed to build local container, attempting existing container.'
		fi
	fi

	if ! "${container_runtime}" image inspect "${ci_registry_image}" 1> '/dev/null'; then
		e_err "Container '${ci_registry_image}' not found, cannot continue."
		exit 1
	fi

	opt_container_args="${OPT_CONTAINER_ARGS:-${OPT_DOCKER_ARGS:-}}"
	opt_container_args="--env 'LOCALVERSION=${LOCALVERSION:-}' ${opt_container_args}"
	opt_container_args="--env 'MAKEFLAGS=-j$(($(nproc) - 1))' ${opt_container_args}"
	opt_container_args="--env 'SHUNIT_COLOR=${SHUNIT_COLOR:-always}' ${opt_container_args}"
	opt_container_args="--volume '$(pwd):${WORKDIR}' ${opt_container_args}"
	opt_container_args="--volume '${fake_whoami}:/usr/local/whoami:ro' ${opt_container_args}"
}

cleanup()
{
	if [ -f "${fake_whoami:-}" ]; then
		rm "${fake_whoami}"
	fi

	trap - EXIT HUP INT QUIT ABRT ALRM TERM
}

container_run()
{
	if [ "${src_name#*'_'}" != "${src_name}" ]; then
		_argv0="${src_dir#"$(pwd)/"}/${src_name#*'_'}"
	else
		usage
		_argv0='/bin/sh'
		e_notice 'Starting shell inside container'
	fi

	eval "${container_runtime}" container run \
	            --hostname "$(cat '/etc/hostname' || true)" \
	            --interactive \
	            --rm \
	            --tty \
	            --workdir "${WORKDIR}" \
	            "${opt_container_args:-}" \
	            "${ci_registry_image}" \
	            "${_argv0:-}" "${*}"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err 'Self-test failed, missing dependencies.'
		e_err '======================================='
		e_err 'Passed tests:'
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		e_err '---------------------------------------'
		e_err 'Failed tests:'
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		e_err '======================================='
		exit 1
	fi
}

main()
{
	_start_time="$(date '+%s')"

	check_requirements
	init
	container_run "${@}"

	echo "Ran container for $(($(date '+%s' || true) - _start_time)) seconds"
}

main "${@}"

exit 0
