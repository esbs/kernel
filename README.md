# ESBS kernel
This repository contains build scripts to build the
[Linux](https://www.kernel.org/) kernel.

The build script can be run using the *-h* parameter for more information about
arguments and environment variables.


## Quickstart
Setup a directory structure as explained below, then
```console
ln -sf "esbs/container_build.sh" "docker_build.sh"
./docker_build.sh -b "My board config" "<platform>_defconfig"
./docker_build.sh
```
The prefix `docker` is used to force `docker` ad the container runtime.


## Using this repository
The make use of the kernel build system, create a new repository, include
this repository as a submodule together with Linux, also as a submodule
and a directory holding configuration files called `configs`. So for example.
```
kernel-project
├── configs
├── esbs
└── linux
```

Building the kernel then is as simple as calling the build script from within
from the esbs optionally passing along the config file.
```console
./esbs/build.sh
```


## Configuration
The `configs` directory contains Linux configuration files. If no configuration
directory is supplied to the build script via the *-b* parameter, the first
entry will be used.

If no config directory exists yet, supplying the *-b* parameter will try to
create one, so it is best to do that in combination with `defconfig`.
```console
./build.sh -b configs/<board> <platform>_defconfig
```
Note, that by default the configs directory is searched for configurations.
However when creating a new configuration, it must be supplied, as any directory
can be used.


### Devicetree
Any devicetree source in the configs directory will be build into a devicetree
binary. These dts files can either be a symlink or include from the Linux source
directory. If no dts files exist, the kernel will build all devicetree source
files belonging to the selected platform. Both *dts* and *dtso* files will be
build into *dtb* and *dtbo* files respectively. The same pre-processor (`cpp`)
as the kernel uses is invoked prior to compiling the devicetree using `dtc`.


### Initramfs
To add basic klibc based initramfs filesystem, a configuration file called
`initramfs.lst` and a directory called `initramfs` is to be created
in the `configs/board/` directory. The regular kernel script
`gen_initramfs_list.sh` is used to create an initramfs from this. If the
flag *-i* is used, the initramfs will be generated as its own file,
`[z]Initramfs` in the `output` directory. Otherwise, it is appended to
the kernel.

A very common layout for the `initramfs` directory could be:
```
kernel-project
├── configs
│   ├── initramfs/init.sh
│   └── initramfs.lst {file /init initramfs/init.sh 0755 0 0}
├── esbs
└── linux
```
Note, that contrary to `gen_initramfs_list.sh`, which will generate
`initramfs.lst` if only a directory is supplied, both need to exist,
as the build script will always create an `initramfs.lst` with the the klibc
entries.

In the config directory, the file `initramfs.lst` is parsed and content is to
be located in the `initramfs` sub-directory. Here an additional `init.sh`
could be defined for example. The build script then appends the `initramfs.lst`
with the klibc files and generates an in kernel initramfs.


## Building
After fetching this repository, there are several methods to build Linux.
Either a local build, which requires all build requirements are resolved
locally, or using for example [Docker](https://www.docker.org) container.

The build script is just a wrapper around the kernel's make command, with some
extras. By default, a [z]Image is build/installed into the output directory. If
other targets are passed to the build script, such as `make modules` the
automatic installation is not performed and files are to be manually obtained.
E.g. passing the correct path to `make install` for example. Generally speaking
those should normally not be used.


## Caches
There are two caches to be aware of when building. One is the *build cache* and
the other the *container cache*.

The container cache is enabled by default, as there is little need to constantly
rebuild the container. If there is a need however to (forcefully) rebuild the
container itself, either locally delete the existing container or better set
`CONTAINER_USE_CACHE` to anything but `yes`.

The build cache is cleared by default. This to ensure reliable builds from
scratch. However as this is quickly very inconvenient, the *-c* flag to the
build script can be passed to keep the existing build cache in `.build`. Please
however be aware of the potential impact re-using the build cache can have.


## Container build
Building using a container is probably the easiest solution to achieve reliable
builds. All that is needed for this is a container to run the build in,
for which there are two possibilities. Either pull the upstream image or locally
generate it. It is recommended to always pull the upstream image, unless
development also requires changes to the build environment itself. By default
however the `docker_build.sh` script will build the container. To use the
upstream image, set the `CI_REGISTRY_IMAGE` variable to point to the image.

While invoking docker directly is certainly possible, it's best to use the
`container-runner.sh` script instead. Without any arguments, the
`container-runner.sh` script just generates and runs the container with a shell.
Passing any arguments to the script will try to run these as programs inside
the container. Best however is use the symlink to the `container-runner.sh`
script, `container_build.sh` which will call `build.sh` within the container
container. Thus calling
```console
./esbs/docker_build.sh help
```
will run **make help** with all requirements set.

Finally, it is most convenient to symlink this script locally.
```console
ln -sf "esbs/container_build.sh" "docker_build.sh"
```
Note, that the local script can have any name, `build.sh` or `kernel_build.sh`
are all fine, as long as they link to `esbs/container_build.sh`, though
recommend is `docker_build.sh`.

The container build scripts potentially support multiple container technologies,
however only tested and supported currently is `docker`. The script will
determine the desired functionality based on either environment variables or
the name of the script.
**Warning**
The above does have as a consequence, filenames part of esbs cannot use an
underscore `_` any longer. e.g. `docker_run_tests.sh` should be renamed to
`docker_run-tests.sh`.

> __Note:__ The container file relies on qemu/binfmt support to properly create
> the container, as it uses klibc for the initramfs. This means a properly
> working host with transparent emulation is needed.
>
> For the debian-like distributions, `qemu-user-static` and `binfmt-support`
> are needed. As an alternative, docker can be used as well, but be aware that
> this is not persistent across reboots.
> `docker run --privileged --rm "index.docker.io/tonistiigi/binfmt" --install "all"`


### Local build
When doing a build without docker, calling the `esbs/build.sh` script will run
make in the linux sub directory passing along any arguments supplied. In other
words, as above with docker, to run **_make menuconfig_** use the script as
`./esbs/build.sh menuconfig`. However this will require all dependencies to be
fulfilled on the local system.

There is a simple script available, `buildenv_check.sh` to verify the basic
needs of the build. Note, that when doing a native build (for example in an arm
environment), the **CROSS_COMPILE** environment variable still needs to be set.

The build environment check currently only checks if the cross compiler is valid
and can cross-compile binaries. For other additional requirements check the
Dockerfile.
