#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu
if [ -n "${DEBUG_TRACE_SH:-}" ] && \
   [ "${DEBUG_TRACE_SH:-}" != "${DEBUG_TRACE_SH#*"$(basename "${0}")"*}" ] || \
   [ "${DEBUG_TRACE_SH:-}" = 'all' ]; then
	set -x
fi

DEF_BUILD_SRC='./linux'
DEF_INITRAMFS_ISOLATE='false'
DEF_KEEP_BUILD_CACHE='false'
DEF_OUTPUT_DIR='output'

INITRAMFS_ROOT_GID="${INITRAMFS_ROOT_GID:-0}"
INITRAMFS_ROOT_UID="${INITRAMFS_ROOT_UID:-0}"
KLIBC_PATH="${KLIBC_PATH:-/usr/lib/klibc-armv7/}"

usage()
{
	echo "Uage: ${0} [OPTIONS] [COMMAND]"
	echo 'This is a wrapper script around the normal Linux Makefile, calling make with [COMMAND].'
	echo 'Its purpose is to make setting of build dirs and passing of configurations easier.'
	echo "    -a  Set the target architecture, an empty var performs a native build (current: '${TARGET_ARCH:-native}'). [TARGET_ARCH]"
	echo '    -b  Build config directory to use [BUILD_CONFIG]'
	echo "    -c  Use previous build cache (default: ${DEF_KEEP_BUILD_CACHE}) [KEEP_BUILD_CACHE]"
	echo '    -d  Build only devicetree(s)'
	echo '    -h  Print usage'
	echo "    -i  Isolate initramfs image to the kernel (default: '${DEF_INITRAMFS_ISOLATE}') [INITRAMFS_ISOLATE]"
	echo "    -o  Set the output directory (default: '${DEF_OUTPUT_DIR}/\${ARCH}/\${BUILD_CONFIG}') [OUTPUT_DIR]"
	echo "    -s  Location of the Linux source directory (default: '${DEF_BUILD_SRC}') [BUILD_SRC]"
	echo '    -x  Override the target compiler, an empty var performs a native build (default: native). [CROSS_COMPILE]'
	echo
	echo 'All options can also be passed in environment variables (listed between [brackets]).'
}

_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

init()
{
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM

	case "${arch}" in
	'aarch64' | 'arm64v8')
		arch='arm64'
		;;
	'amd64')
		arch='x86_64'
		;;
	'arm'*)
		arch='arm'
		;;
	*)
		;;
	esac

	if [ -z "${MAKEFLAGS:-}" ]; then
		MAKEFLAGS="-j$(($(nproc) - 1))"
		e_warn "MAKEFLAGS where not set, setting them to '${MAKEFLAGS}'."
		export MAKEFLAGS
	fi

	if [ "${keep_build_cache}" != 'true' ] && \
	   [ -d "${build_dir}" ]; then
		rm -f -r "${build_dir:?}"
	fi
	mkdir -p "${build_dir}"

	if [ -z "${LOCALVERSION:-}" ] && git rev-parse --show-cdup 2> '/dev/null'; then
		if ! git describe 2> '/dev/null'; then
			LOCALVERSION='g'
		fi
		LOCALVERSION="-${LOCALVERSION:-}$(git describe --always --dirty)"
		export LOCALVERSION
	fi

	if [ -n "${output_dir##'/'*}" ]; then
		output_dir="$(pwd)/${output_dir}"
	fi
	if [ -d "${output_dir}" ]; then
		rm -f -r "${output_dir:?}"
	fi
	mkdir -p "${output_dir}/boot"

	for _dts in "${build_config}/"*'.dts' "${build_config}/"*'.dtso'; do
		if [ ! -f "${_dts}" ]; then
			continue
		fi
		devicetrees="$(readlink -f "${_dts}") ${devicetrees:-}";
	done
	if [ -f "${build_config}/linux.config" ]; then
		linux_config="$(readlink -f "${build_config}/linux.config")"
	else
		if [ -n "${build_config##'/'*}" ]; then
			_pwd="$(pwd)/"
		fi
		linux_config="${_pwd:-}${build_config}/linux.config"
		return
	fi
	# The following code requires a valid config and so should not be used
	# in other cases, e.g. when creating a new config via defconfig.

	if [ ! -f "${build_dir}/include/config/auto.conf" ]; then
		build_kernel 'prepare'
	fi
	kernel_release="$(build_kernel -s 'kernelrelease')"

	_archive_compressions='
		BZIP2
		GZIP
		LZ4
		LZMA
		LZO
		NONE
		XZ
	'

	for _archive_compression in ${_archive_compressions}; do
		if grep -q "^CONFIG_KERNEL_${_archive_compression}=y$" "${linux_config}"; then
			# shellcheck disable=SC2021  # Busybox tr not POSIX character classes
			compressed="$(echo "${_archive_compression}" | tr '[A-Z]' '[a-z]')"
			if [ "${compressed}" = 'none' ]; then
				compressed=""
			fi
			break
		fi
	done

	if [ "${initramfs_isolate}" != 'true' ]; then
		# shellcheck disable=SC2021
		sed -i "/^CONFIG_INITRAMFS_SOURCE=.*/aCONFIG_INITRAMFS_COMPRESSION_$(echo "${_archive_compression:-NONE}" | \
		                                                                       tr -d '.' || true| \
		                                                                       tr '[a-z]' '[A-Z]' || true)=y" "${linux_config}"
		for _available_compression in ${_archive_compressions}; do
			if [ "${_available_compression}" = "${_archive_compression:-NONE}" ]; then
				sed -i "/^CONFIG_INITRAMFS_SOURCE=.*/aCONFIG_INITRAMFS_COMPRESSION=\"${compressed:-none}\"" "${linux_config}"
			else
				sed -i "/^CONFIG_INITRAMFS_SOURCE=.*/a# CONFIG_INITRAMFS_COMPRESSION_${_available_compression} is not set" "${linux_config}"
			fi
		done
		sed -i "/^CONFIG_INITRAMFS_SOURCE=.*/aCONFIG_INITRAMFS_ROOT_GID=${INITRAMFS_ROOT_GID}" "${linux_config}"
		sed -i "/^CONFIG_INITRAMFS_SOURCE=.*/aCONFIG_INITRAMFS_ROOT_UID=${INITRAMFS_ROOT_UID}" "${linux_config}"
		sed -i "s|^CONFIG_INITRAMFS_SOURCE=.*|CONFIG_INITRAMFS_SOURCE=\"${build_dir}/initramfs.lst\"|g" "${linux_config}"
	fi
}

cleanup()
{
	if [ "${keep_build_cache}" != 'true' ] && \
	   [ -d "${build_dir}" ]; then
		rm -f -r "${build_dir:?}"
	fi

	if [ -f "${linux_config:-}" ] && \
	   [ "${initramfs_isolate}" != 'true' ]; then
		sed -i '/CONFIG_INITRAMFS_COMPRESSION/d' "${linux_config}"
		sed -i '/^CONFIG_INITRAMFS_ROOT_[UG]ID=/d' "${linux_config}"
		sed -i 's|^CONFIG_INITRAMFS_SOURCE=.*|CONFIG_INITRAMFS_SOURCE=""|g' "${linux_config}"
	fi

	trap - EXIT HUP INT QUIT ABRT ALRM TERM
}

prepare_initramfs()
{
	mkdir -p "${build_dir}/initramfs/"
	if [ -d "${build_config}/initramfs/" ]; then
		cp -a -T "${build_config}/initramfs/" "${build_dir}/initramfs/"
	fi

	if [ -f "${build_config}/initramfs.lst" ]; then
		cp -a "${build_config}/initramfs.lst" "${build_dir}/initramfs.lst"
	fi

	if [ ! -x "${build_dir}/usr/gen_init_cpio" ]; then
		build_kernel 'usr/'
	fi

	if [ ! -d "${KLIBC_PATH}/bin" ]; then
		e_err 'No KLIBC binaries found'
		exit 1
	fi

	echo >> "${build_dir}/initramfs.lst"
	echo '# Klibc utils' >> "${build_dir}/initramfs.lst"

	install -D -d -m 0755 "${build_dir}/initramfs/bin"
	echo 'dir /bin 0755 0 0' >> "${build_dir}/initramfs.lst"

	for _cmd in "${KLIBC_PATH}/bin/"*; do
		if [ ! -f "${_cmd}" ] || [ ! -x "${_cmd}" ]; then
			continue
		fi

		if ! "${cross_compile}objdump" -a "${_cmd}" 1> '/dev/null'; then
			e_err 'Incompatible Klibc, architecture mismatch, point KLIBC_PATH to compatible klibc-utils.'
			exit 1
		fi

		install -D -m 0755 -t "${build_dir}/initramfs/bin/" "${_cmd}"
		if [ "${_cmd%'.shared'}" != "${_cmd}" ]; then
			mv "${build_dir}/initramfs/bin/${_cmd}" "${build_dir}/initramfs/bin/${_cmd%'.shared'}"
		fi

		_base_cmd="$(basename "${_cmd%'.shared'}")"
		echo "file /bin/${_base_cmd} initramfs/bin/${_base_cmd} 0755 0 0" >> "${build_dir}/initramfs.lst"
	done

	install -D -d -m 0755 "${build_dir}/initramfs/lib"
	echo 'dir /lib 0755 0 0' >> "${build_dir}/initramfs.lst"

	for _interpreter in "${KLIBC_PATH}/lib/klibc-"*'.so'; do
		if [ ! -f "${_interpreter}" ]; then
		    continue
		fi

		if ! "${cross_compile}objdump" -a "${_interpreter}" 1> '/dev/null'; then
		    e_err 'Incompatible Klibc library, architecture mismatch, point KLIBC_PATH to a compatible Klibc library.'
		    exit 1
		fi
		install -D -m 0755 -t "${build_dir}/initramfs/lib/" "${_interpreter}"
		echo "file /lib/${_interpreter##*'/'} initramfs/lib/${_interpreter##*'/'} 0755 0 0" >> "${build_dir}/initramfs.lst"
	done
}

install_initramfs()
{
	(
		cd "${build_dir}"
		"${build_src}/usr/gen_initramfs_list.sh" \
		                                         -o "${build_dir}/initramfs.cpio${compressed:+.${compressed}}" \
		                                         -g "${INITRAMFS_ROOT_GID}" \
		                                         -u "${INITRAMFS_ROOT_UID}" \
		                                         'initramfs.lst'
	)
	if [ ! -f "${build_dir}/initramfs.cpio${compressed:+.${compressed}}" ]; then
		e_err 'Failed to create requested initramfs'
		exit 1
	fi

	install -D -m 0644 \
	        "${build_dir}/initramfs.cpio${compressed:+.${compressed}}" \
	        "${output_dir}/boot/${compressed:+z}Initramfs-${kernel_release}"
}

build_kernel()
{
	ARCH="${arch}" \
	CROSS_COMPILE="${cross_compile}" \
	nice -n 19 \
	     make -C "${build_src}" \
	     INSTALL_MOD_PATH="${output_dir}" \
	     INSTALL_PATH="${output_dir}/boot/" \
	     KCONFIG_CONFIG="${linux_config}" \
	     O="${build_dir}" \
	     "${@}"
}

build_devicetrees()
{
	mkdir -p "${build_dir}/arch/arm/boot/dts/"

	for _dts in ${devicetrees:-}; do
		if [ ! -f "${_dts}" ]; then
			continue
		fi

		_overlay=''
		if [ "${_dts%%'o'}" != "${_dts}" ]; then
			_overlay='o'
		fi
		_dt_filename="$(basename "${_dts%".dts${_overlay}"}")"
		echo "Building devicetree blob '${_dt_filename}'"
		cpp \
		    -D__DTS__ \
		    -nostdinc \
		    -undef \
		    -x assembler-with-cpp \
		    -I "${build_src}/include" \
		    -I "${build_src}/arch/arm/boot/dts" \
		    -o "${build_dir}/arch/arm/boot/dts/.${_dt_filename}.dtb${_overlay}.tmp" \
		    "${_dts}"
		dtc \
		    -O dtb \
		    -o "${build_dir}/arch/arm/boot/dts/${_dt_filename}.dtb${_overlay}" \
		    -I dts \
		    -@ \
		    "${build_dir}/arch/arm/boot/dts/.${_dt_filename}.dtb${_overlay}.tmp"
	done
}

build_all()
{
	echo "Installing into '${output_dir}'."
	if [ -z "${devicetrees_only:-}" ]; then
		build_kernel "${compressed:+z}Image"
		build_kernel 'install'
		build_kernel 'modules'
		build_kernel 'modules_install'

		if [ -L "${output_dir}/lib/modules/${kernel_release}/build" ]; then
			unlink "${output_dir}/lib/modules/${kernel_release}/build"
		fi
		if [ -L "${output_dir}/lib/modules/${kernel_release}/source" ]; then
			unlink "${output_dir}/lib/modules/${kernel_release}/source"
		fi
	fi
	if [ -z "${devicetrees}" ]; then
		build_kernel 'dtbs'
	else
		build_devicetrees
	fi

	find "${build_dir}/arch/arm/boot/dts/" \
	     -type f \
	     \( -iname '*.dtb' -o -iname '*.dtbo' \) \
	     -exec install -D \
	                   -m 0644 \
	                   -t "${output_dir}/boot/dtbs/${kernel_release}" \
			   '{}' \;
}

main()
{
	_start_time="$(date '+%s')"

	while getopts ':a:b:cdhio:s:x:' _options; do
		case "${_options}" in
		'a')
			arch="${OPTARG}"
			;;
		'b')
			build_config="$(echo "${OPTARG}" | sed 's|/*$||g')"
			;;
		'c')
			keep_build_cache='true'
			;;
		'd')
			devicetrees_only='true';
			;;
		'h')
			usage
			exit 0
			;;
		'i')
			initramfs_isolate='true'
			;;
		'o')
			output_dir="${OPTARG}"
			;;
		's')
			build_src="${OPTARG}"
			;;
		'x')
			cross_compile="${OPTARG}"
			;;
		':')
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		*)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ -z "${build_config:=${BUILD_CONFIG:-}}" ]; then
		for _config in 'configs/'*; do
			if [ -d "${_config}" ] && \
			   [ -f "${_config}/linux.config" ]; then
				build_config="${_config}"
				break
			fi
		done
	fi

	arch="${arch:-${TARGET_ARCH:-$(uname -m)}}"
	build_name="${build_config##*'/'}"
	build_dir="${BUILD_DIR:-$(pwd)/.build/${arch}/${build_name}}"
	build_src="$(readlink -f "${build_src:-${BUILD_SRC:-${DEF_BUILD_SRC}}}")"
	cross_compile="${cross_compile:-${CROSS_COMPILE:-}}"
	initramfs_isolate="${initramfs_isolate:-${INITRAMFS_ISOLATE:-${DEF_INITRAMFS_ISOLATE}}}"
	keep_build_cache="${keep_build_cache:-${KEEP_BUILD_CACHE:-${DEF_KEEP_BUILD_CACHE}}}"
	output_dir="${output_dir:-${OUTPUT_DIR:-$(pwd)/${DEF_OUTPUT_DIR}/${arch}/${build_name}}}"

	if [ ! -f "${build_config}/linux.config" ]; then
		e_warn "Missing build configuration '${build_config}'."
		printf 'Create new one? (y/n): '
		read -r __sure
		echo
		if [ "${__sure}" != 'y' ] && [ "${__sure}" != 'Y' ]; then
			e_err 'Cannot continue without configuration, aborting.'
			exit 1
		fi
	fi

	init

	case "${@}" in
	'all'*)
		# Rely on the internal scripts 'all'
		shift "${#}"
		;;
	*'_defconfig')
		build_kernel "${@}"
		exit 0
		;;
	*'help'*)
		build_kernel 'help'
		exit 0
		;;
	*'menuconfig'*)
		build_kernel 'menuconfig'
		exit 0
		;;
	*)
		;;
	esac

	echo "Building for target '${build_name}', '${kernel_release:=unknown}'."

	if [ -d "${build_config}/initramfs" ] || \
	   [ -f "${build_config}/initramfs.lst" ]; then
		prepare_initramfs

		if [ "${initramfs_isolate}" = 'true' ]; then
			install_initramfs
		fi
	fi

	if [ "${#}" -gt 0 ]; then
		build_kernel "${@}"
	else
		build_all
	fi

	echo '==============================================================================='
	echo "Build report for $(date -u || true)"
	echo "Version: Linux ${kernel_release}"
	echo
	if [ -f "${output_dir}/boot/vmlinux-${kernel_release}" ]; then
		printf "Linux Image size:	%7s\n" \
		       "$(du -d 0 -h "$(readlink -f "${output_dir}/boot/vmlinux-${kernel_release}" || true)" || true | \
		          cut -f 1 || true)"
	fi
	if [ -f "${output_dir}/boot/vmlinuz-${kernel_release}" ]; then
		printf "Linux Image size:	%7s\n" \
		       "$(du -d 0 -h "$(readlink -f "${output_dir}/boot/vmlinuz-${kernel_release}" || true)" || true | \
		          cut -f 1 || true)"
	fi
	if [ -f "${output_dir}/boot/Initramfs-${kernel_release}" ]; then
		printf "Initramfs size:	%7s\n" \
		       "$(du -d 0 -h "$(readlink -f "${output_dir}/boot/Initramfs-${kernel_release}" || true)" || true | \
		          cut -f 1 || true)"
	fi
	if [ -f "${output_dir}/boot/zInitramfs-${kernel_release}" ]; then
		printf "zInitramfs size:	%7s\n" \
		       "$(du -d 0 -h "$(readlink -f "${output_dir}/boot/zInitramfs-${kernel_release}" || true)" || true | \
		          cut -f 1 || true)"
	fi
	if [ -d "${output_dir}/lib/modules/${kernel_release}" ]; then
		printf "Linux modules size:	%7s\n" \
		       "$(du -d 0 -h "$(readlink -f "${output_dir}/lib/modules/${kernel_release}" || true)" | \
		          cut -f 1 || true)"
	fi
	if [ -d "${output_dir}/boot/dtbs/${kernel_release}" ]; then
		printf "Devicetree blob sizes:	%7s\n" \
		       "$(du -d 0 -h "$(readlink -f "${output_dir}/boot/dtbs/${kernel_release}" || true)" || true| \
		          cut -f 1 || true)"
	fi
	echo
	echo "Successfully built Linux for '${build_name}' in $(($(date '+%s' || true) - _start_time)) seconds."
	echo '==============================================================================='

	cleanup
}

main "${@}"

exit 0
