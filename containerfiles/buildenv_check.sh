#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu
if [ -n "${DEBUG_TRACE_SH:-}" ] && \
   [ "${DEBUG_TRACE_SH:-}" != "${DEBUG_TRACE_SH#*"$(basename "${0}")"*}" ] || \
   [ "${DEBUG_TRACE_SH:-}" = 'all' ]; then
	set -x
fi

CROSS_COMPILE="${CROSS_COMPILE:-}"
REQUIRED_COMMANDS='
	[
	bc
	bison
	break
	bzip2
	cat
	command
	cp
	cpp
	date
	dc
	dtc
	du
	echo
	exit
	find
	flex
	getopts
	git
	grep
	gzip
	install
	lz4
	lzma
	lzop
	make
	mkdir
	mktemp
	nice
	nproc
	perl
	printf
	readlink
	rm
	shift
	test
	tr
	trap
	uname
	unlink
	wc
	which
	xz
'

usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo 'A simple test script to verify the environment has all required tools.'
	echo '    -h  Print usage'
}

init()
{
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM

	test_dir_template='buildenv_check.XXXXXX'
	test_dir="$(mktemp -d "${test_dir_template}")"
}

cleanup()
{
	if [ -d "${test_dir:-}" ] && \
	   [ -z "${test_dir##*"${test_dir_template}"*}" ]; then
		rm -f -r "${test_dir:?}/"
	fi

	trap - EXIT HUP INT QUIT ABRT ALRM TERM
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}" 2>&1)"; then
			_test_result_fail="${_test_result_fail:-}${_test_result#*'command: '}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	echo 'Available commands:'
	# As the results contain \n, we expect these to be interpreted.
	# shellcheck disable=SC2059
	printf "${_test_result_pass:-none\n}"
	echo
	echo 'Missing commands:'
	# shellcheck disable=SC2059
	printf "${_test_result_fail:-none\n}"
	echo

	if [ -n "${_test_result_fail:-}" ]; then
		echo 'Command test failed, missing programs.'
		test_failed=1
	fi
}

check_compiler()
{
	cat <<-EOT > "${test_dir}/compiler_test.c"
		int main(void)
		{
		    return 0;
		}
	EOT

	"${CROSS_COMPILE}gcc" -Wall -Werror \
	                      -c "${test_dir}/compiler_test.c" \
	                      -o "${test_dir}/compiler_test" || \
	                     _compile_failed=1

	"${CROSS_COMPILE}objdump" -S -d \
	                          "${test_dir}/compiler_test" 1> '/dev/null' || \
	                          _compile_failed=1

	printf 'compiler support: '
	if [ -n "${_compile_failed:-}" ]; then
		test_failed=1
		echo 'error'
	else
		echo 'ok'
	fi
}

main()
{
	while getopts ':h' _options; do
		case "${_options}" in
		'h')
			usage
			exit 0
			;;
		':')
			echo "Option -${OPTARG} requires an argument."
			exit 1
			;;
		*)
			echo "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	check_requirements
	init
	check_compiler

	if [ -n "${test_failed:-}" ]; then
		echo 'Test(s) failed.'
		exit 1
	fi

	echo 'All ok'

	cleanup
}

main "${@}"

exit 0
